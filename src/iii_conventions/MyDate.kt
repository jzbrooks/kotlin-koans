package iii_conventions

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override fun compareTo(other: MyDate): Int {
        return when {
            this.year != other.year -> this.year - other.year
            this.month != other.month -> this.month - other.month
            else -> this.dayOfMonth - other.dayOfMonth
        }
    }
}

operator fun MyDate.rangeTo(other: MyDate): DateRange = DateRange(this, other)
operator fun MyDate.plus(interval: TimeInterval) : MyDate { return this.addTimeIntervals(interval, 1) }
operator fun MyDate.plus(repeatedInterval: RepeatedTimeInterval) : MyDate {
    return this.addTimeIntervals(repeatedInterval.interval, repeatedInterval.quantity)
}

enum class TimeInterval {
    DAY,
    WEEK,
    YEAR
}

class RepeatedTimeInterval(val interval: TimeInterval, val quantity : Int)

operator fun TimeInterval.times(quantity: Int) : RepeatedTimeInterval {
    return RepeatedTimeInterval(this, quantity)
}


class DateRange(val start: MyDate, val endInclusive: MyDate) : Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> {
        return object : Iterator<MyDate> {
            var current = start.addTimeIntervals(TimeInterval.DAY, -1)

            override fun next(): MyDate {
                val next = current.nextDay()
                current = next

                return next
            }

            override fun hasNext(): Boolean {
                return current.nextDay() <= endInclusive
            }
        }
    }
}

operator fun DateRange.contains(myDate: MyDate) : Boolean {
    return myDate > start && myDate <= endInclusive
}

